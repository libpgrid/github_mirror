/*
 * This file is part of the pgrid project.
 *
 * Copyright (c) 2012. Vourlakis Nikolas. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package pgrid.service.corba.repair;


/**
 * pgrid/service/corba/repair/RepairSolutionHelper.java .
 * Generated by the IDL-to-Java compiler (portable), version "3.2"
 * from resources/pgrid_corba.idl
 * Tuesday, January 24, 2012 7:07:45 PM EET
 */

abstract public class RepairSolutionHelper {
    private static String _id = "IDL:pgrid/service/corba/repair/RepairSolution:1.0";

    public static void insert(org.omg.CORBA.Any a, pgrid.service.corba.repair.RepairSolution that) {
        org.omg.CORBA.portable.OutputStream out = a.create_output_stream();
        a.type(type());
        write(out, that);
        a.read_value(out.create_input_stream(), type());
    }

    public static pgrid.service.corba.repair.RepairSolution extract(org.omg.CORBA.Any a) {
        return read(a.create_input_stream());
    }

    private static org.omg.CORBA.TypeCode __typeCode = null;
    private static boolean __active = false;

    synchronized public static org.omg.CORBA.TypeCode type() {
        if (__typeCode == null) {
            synchronized (org.omg.CORBA.TypeCode.class) {
                if (__typeCode == null) {
                    if (__active) {
                        return org.omg.CORBA.ORB.init().create_recursive_tc(_id);
                    }
                    __active = true;
                    org.omg.CORBA.StructMember[] _members0 = new org.omg.CORBA.StructMember[5];
                    org.omg.CORBA.TypeCode _tcOf_members0 = null;
                    _tcOf_members0 = org.omg.CORBA.ORB.init().create_string_tc(0);
                    _tcOf_members0 = org.omg.CORBA.ORB.init().create_sequence_tc(0, _tcOf_members0);
                    _tcOf_members0 = org.omg.CORBA.ORB.init().create_alias_tc(pgrid.service.corba.StringArrayHelper.id(), "StringArray", _tcOf_members0);
                    _members0[0] = new org.omg.CORBA.StructMember(
                            "uuidSent",
                            _tcOf_members0,
                            null);
                    _tcOf_members0 = org.omg.CORBA.ORB.init().get_primitive_tc(org.omg.CORBA.TCKind.tk_long);
                    _members0[1] = new org.omg.CORBA.StructMember(
                            "pushCount",
                            _tcOf_members0,
                            null);
                    _tcOf_members0 = pgrid.service.corba.PeerReferenceHelper.type();
                    _tcOf_members0 = org.omg.CORBA.ORB.init().create_sequence_tc(0, _tcOf_members0);
                    _tcOf_members0 = org.omg.CORBA.ORB.init().create_alias_tc(pgrid.service.corba.PeerArrayHelper.id(), "PeerArray", _tcOf_members0);
                    _members0[2] = new org.omg.CORBA.StructMember(
                            "failedHosts",
                            _tcOf_members0,
                            null);
                    _tcOf_members0 = pgrid.service.corba.PeerReferenceHelper.type();
                    _tcOf_members0 = org.omg.CORBA.ORB.init().create_sequence_tc(0, _tcOf_members0);
                    _tcOf_members0 = org.omg.CORBA.ORB.init().create_alias_tc(pgrid.service.corba.PeerArrayHelper.id(), "PeerArray", _tcOf_members0);
                    _members0[3] = new org.omg.CORBA.StructMember(
                            "updatedHosts",
                            _tcOf_members0,
                            null);
                    _tcOf_members0 = org.omg.CORBA.ORB.init().create_string_tc(0);
                    _members0[4] = new org.omg.CORBA.StructMember(
                            "failedPath",
                            _tcOf_members0,
                            null);
                    __typeCode = org.omg.CORBA.ORB.init().create_struct_tc(pgrid.service.corba.repair.RepairSolutionHelper.id(), "RepairSolution", _members0);
                    __active = false;
                }
            }
        }
        return __typeCode;
    }

    public static String id() {
        return _id;
    }

    public static pgrid.service.corba.repair.RepairSolution read(org.omg.CORBA.portable.InputStream istream) {
        pgrid.service.corba.repair.RepairSolution value = new pgrid.service.corba.repair.RepairSolution();
        value.uuidSent = pgrid.service.corba.StringArrayHelper.read(istream);
        value.pushCount = istream.read_long();
        value.failedHosts = pgrid.service.corba.PeerArrayHelper.read(istream);
        value.updatedHosts = pgrid.service.corba.PeerArrayHelper.read(istream);
        value.failedPath = istream.read_string();
        return value;
    }

    public static void write(org.omg.CORBA.portable.OutputStream ostream, pgrid.service.corba.repair.RepairSolution value) {
        pgrid.service.corba.StringArrayHelper.write(ostream, value.uuidSent);
        ostream.write_long(value.pushCount);
        pgrid.service.corba.PeerArrayHelper.write(ostream, value.failedHosts);
        pgrid.service.corba.PeerArrayHelper.write(ostream, value.updatedHosts);
        ostream.write_string(value.failedPath);
    }

}
